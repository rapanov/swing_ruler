/**
 * Copyright (C) 2019, Roman Panov (roman.a.panov@gmail.com).
 */

package org.panovrom.intellij.ruler;

import java.awt.Dialog;
import java.awt.EventQueue;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class LayoutRulerManager
{
  private static LayoutRulerManager ourManager = null;
  private final WindowListener myWindowListener = new WindowListener();
  private final Set<Window> myWindows = new HashSet<Window>();
  private final Map<Window, LayoutRuler> myRulers = new HashMap<Window, LayoutRuler>();

  static LayoutRulerManager getManager()
  {
    assert EventQueue.isDispatchThread();

    if (ourManager == null)
    {
      ourManager = new LayoutRulerManager();
    }

    return ourManager;
  }

  void enable()
  {
    attach(getActiveWindow());
  }

  void disable()
  {
    Window activeWindow = null;

    for (final Map.Entry<Window, LayoutRuler> entry : myRulers.entrySet())
    {
      entry.getValue().detach();

      final Window window = entry.getKey();

      if (window.isActive())
      {
        activeWindow = window;
      }
    }

    myRulers.clear();

    for (final Window window : myWindows)
    {
      window.removeWindowListener(myWindowListener);
    }

    if (activeWindow != null)
    {
      activeWindow.repaint();
    }
  }

  private LayoutRulerManager()
  {}

  private void attach(final Window window)
  {
    if (window == null)
    {
      return;
    }

    boolean isListenerAttached = false;

    for (final Object listener : window.getWindowListeners())
    {
      if (listener == myWindowListener)
      {
        isListenerAttached = true;
        break;
      }
    }

    if (!isListenerAttached)
    {
      window.addWindowListener(myWindowListener);
      myWindows.add(window);
    }

    if (!(window instanceof Dialog))
    {
      // Attach only to dialogs.
      return;
    }

    final LayoutRuler ruler = myRulers.get(window);
    final boolean hasRuler = ruler != null;
    final boolean hasAttachedRuler = hasRuler && !ruler.isDetached();

    if (hasAttachedRuler)
    {
      return;
    }

    final LayoutRuler newRuler = LayoutRuler.attach(window);

    if (newRuler == null && hasRuler)
    {
      // Remove detached ruler.
      myRulers.remove(window);
    }
  }

  private static Window getActiveWindow()
  {
    final Window[] windows = Window.getWindows();

    for (final Window window : windows)
    {
      if (window.isActive())
      {
        return window;
      }
    }

    return null;
  }

  private final class WindowListener extends WindowAdapter
  {
    @Override
    public void windowDeactivated(final WindowEvent e)
    {
      LayoutRulerManager.this.attach(e.getOppositeWindow());
    }
  }
}

