/**
 * Copyright (C) 2019, Roman Panov (roman.a.panov@gmail.com).
 */

package org.panovrom.intellij.ruler;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.ToggleAction;
import java.awt.EventQueue;

public final class LayoutRulerAction extends ToggleAction
{
  private boolean myIsEnabled = false;

  @Override
  public boolean isSelected(final AnActionEvent event)
  {
    return myIsEnabled;
  }

  @Override
  public void setSelected(final AnActionEvent event, final boolean selected)
  {
    assert EventQueue.isDispatchThread();

    if (myIsEnabled == selected)
    {
      return;
    }

    final LayoutRulerManager manager = LayoutRulerManager.getManager();

    if (selected)
    {
      manager.enable();
    }
    else
    {
      manager.disable();
    }

    myIsEnabled = selected;
  }
}
