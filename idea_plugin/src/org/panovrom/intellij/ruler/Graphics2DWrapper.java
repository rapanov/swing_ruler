/**
 * Copyright (C) 2014-2016, Roman Panov (roman.a.panov@gmail.com).
 */

package org.panovrom.intellij.ruler;

import java.awt.Color;
import java.awt.Composite;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.font.FontRenderContext;
import java.awt.font.GlyphVector;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ImageObserver;
import java.awt.image.RenderedImage;
import java.awt.image.renderable.RenderableImage;
import java.text.AttributedCharacterIterator;
import java.util.Map;

final class Graphics2DWrapper extends Graphics2D
{
  private final Graphics2D myPeer;
  private final AffineTransform myTransform;
  private final RenderingHints myRenderingHints;
  private final Rectangle myTmpRect0;
  private Rectangle myTmpRect1 = null;
  private Rectangle myClipBounds;
  private Color myColor;
  private Font myFont;
  private Composite myComposite;

  private Paint myPaint;
  private Stroke myStroke;
  private Color myBackground;
  private final SharedData mySharedData;

  Graphics2DWrapper(final Graphics2D peer)
  {
    this(peer, null);
  }

  Graphics2DWrapper(final Graphics2D peer, final SharedData sharedData)
  {
    if (peer == null)
    {
      throw new NullPointerException();
    }

    myPeer = peer;
    myTransform = new AffineTransform(peer.getTransform());
    myRenderingHints = (RenderingHints)peer.getRenderingHints().clone();
    myTmpRect0 = new Rectangle();
    myClipBounds = peer.getClipBounds(myTmpRect0);
    myColor = peer.getColor();
    myFont = peer.getFont();
    myComposite = peer.getComposite();
    myPaint = peer.getPaint();
    myStroke = peer.getStroke();
    myBackground = peer.getBackground();
    mySharedData = sharedData == null ? new SharedData() : sharedData;
  }

  @Override
  public Graphics create()
  {
    return new Graphics2DWrapper(this, mySharedData);
  }

  @Override
  public Graphics create(final int x, final int y, final int width, final int height)
  {
    final Graphics2DWrapper newGraphics = new Graphics2DWrapper(this, mySharedData);
    newGraphics.clipRect(x, y, width, height);
    newGraphics.myTransform.translate(x, y);
    newGraphics.myClipBounds.translate(-x, -y);
    return newGraphics;
  }

  @Override
  public void translate(final int x, final int y)
  {
    myTransform.translate(x, y);
  }

  @Override
  public Color getColor()
  {
    return myColor;
  }

  @Override
  public void setColor(final Color color)
  {
    myColor = color;
  }

  @Override
  public void setPaintMode()
  {}

  @Override
  public void setXORMode(final Color color)
  {}

  @Override
  public Font getFont()
  {
    return myFont;
  }

  @Override
  public void setFont(final Font font)
  {
    myFont = font;
  }

  @Override
  public FontMetrics getFontMetrics()
  {
    return myPeer.getFontMetrics();
  }

  @Override
  public FontMetrics getFontMetrics(final Font font)
  {
    return myPeer.getFontMetrics(font);
  }

  @Override
  public Rectangle getClipBounds()
  {
    return myClipBounds == null ? null : new Rectangle(myClipBounds);
  }

  @Override
  public void clipRect(final int x, final int y, final int width, final int height)
  {
    if (myClipBounds == null)
    {
      myClipBounds = myTmpRect0;
      myClipBounds.setBounds(x, y, width, height);
    }

    if (myTmpRect1 == null)
    {
      myTmpRect1 = new Rectangle();
    }

    myTmpRect1.setBounds(x, y, width, height);
    myClipBounds.setBounds(myClipBounds.intersection(myTmpRect1));
  }

  @Override
  public void setClip(final int x, final int y, final int width, final int height)
  {
    if (myClipBounds == null)
    {
      myClipBounds = myTmpRect0;
    }

    myClipBounds.setBounds(x, y, width, height);
  }

  @Override
  public Shape getClip()
  {
    return myClipBounds == null ? null : new Rectangle(myClipBounds);
  }

  @Override
  public void setClip(final Shape clip)
  {
    if (clip == null)
    {
      myClipBounds = null;
    }
    else
    {
      final Rectangle clipBounds = clip.getBounds();

      if (clipBounds == null)
      {
        myClipBounds = null;
      }
      else
      {
        myClipBounds.setBounds(clipBounds);
      }
    }
  }

  @Override
  public void copyArea(
      final int x, final int y, final int width, final int height, final int dx, final int dy)
  {}

  @Override
  public void drawLine(final int x1, final int y1, final int x2, final int y2)
  {}

  @Override
  public void fillRect(final int x, final int y, final int width, final int height)
  {}

  @Override
  public void drawRect(final int x, final int y, final int width, final int height)
  {}

  @Override
  public void clearRect(final int x, final int y, final int width, final int height)
  {}

  @Override
  public void drawRoundRect(
      final int x, final int y,
      final int width, final int height,
      final int arcWidth, final int arcHeight)
  {}

  @Override
  public void fillRoundRect(
      final int x, final int y,
      final int width, final int height,
      final int arcWidth, final int arcHeight)
  {}

  @Override
  public void draw3DRect(
      final int x, final int y,
      final int width, final int height,
      final boolean raised)
  {}

  @Override
  public void fill3DRect(
      final int x, final int y,
      final int width, final int height,
      final boolean raised)
  {}

  @Override
  public void drawOval(final int x, final int y, final int width, final int height)
  {}

  @Override
  public void fillOval(final int x, final int y, final int width, final int height)
  {}

  @Override
  public void drawArc(
      final int x, final int y,
      final int width, final int height,
      final int startAngle, final int arcAngle)
  {}

  @Override
  public void fillArc(
      final int x, final int y,
      final int width, final int height,
      final int startAngle, final int arcAngle)
  {}

  @Override
  public void drawPolyline(final int[] xPoints, final int[] yPoints, final int nPoints)
  {}

  @Override
  public void drawPolygon(final int[] xPoints, final int[] yPoints, final int nPoints)
  {}

  @Override
  public void drawPolygon(final Polygon p)
  {}

  @Override
  public void fillPolygon(final int[] xPoints, final int[] yPoints, final int nPoints)
  {}

  @Override
  public void fillPolygon(final Polygon p)
  {}

  @Override
  public void drawString(final String str, final int x, final int y)
  {
    drawText(x, y);
  }

  @Override
  public void drawString(final AttributedCharacterIterator iterator, final int x, final int y)
  {
    drawText(x, y);
  }

  @Override
  public void drawChars(
      final char[] data,
      final int offset, final int length,
      final int x, final int y)
  {
    drawText(x, y);
  }

  @Override
  public void drawBytes(
      final byte[] data, final int offset, final int length, final int x, final int y)
  {
    drawText(x, y);
  }

  @Override
  public boolean drawImage(
      final Image img,
      final int x, final int y,
      final ImageObserver observer)
  {
    return true;
  }

  @Override
  public boolean drawImage(
      final Image img,
      final int x, final int y,
      final int width, final int height,
      final ImageObserver observer)
  {
    return true;
  }

  @Override
  public boolean drawImage(
      final Image img,
      final int x, final int y,
      final Color bgColor, final ImageObserver observer)
  {
    return true;
  }

  @Override
  public boolean drawImage(
      final Image img,
      final int x, final int y,
      final int width, final int height,
      final Color bgColor, final ImageObserver observer)
  {
    return true;
  }

  @Override
  public boolean drawImage(
      final Image img,
      final int dx1, final int dy1, final int dx2, final int dy2,
      final int sx1, final int sy1, final int sx2, final int sy2,
      final ImageObserver observer)
  {
    return true;
  }

  @Override
  public boolean drawImage(
      final Image img,
      final int dx1, final int dy1, final int dx2, final int dy2,
      final int sx1, final int sy1, final int sx2, final int sy2,
      final Color bgcolor, final ImageObserver observer)
  {
    return true;
  }

  @Override
  public void dispose()
  {}

  @Override
  public Rectangle getClipRect()
  {
    return getClipBounds();
  }

  @Override
  public boolean hitClip(final int x, final int y, final int width, final int height)
  {
    if (myClipBounds == null)
    {
      return true;
    }

    if (myTmpRect1 == null)
    {
      myTmpRect1 = new Rectangle();
    }

    myTmpRect1.setBounds(x, y, width, height);
    return myClipBounds.intersects(myTmpRect1);
  }

  @Override
  public Rectangle getClipBounds(final Rectangle r)
  {
    if (myClipBounds != null)
    {
      r.setBounds(myClipBounds);
    }

    return r;
  }

  @Override
  public void draw(final Shape s)
  {}

  @Override
  public boolean drawImage(
      final Image img, final AffineTransform xform, final ImageObserver observer)
  {
    return true;
  }

  @Override
  public void drawImage(
      final BufferedImage img,
      final BufferedImageOp op,
      final int x, final int y)
  {}

  @Override
  public void drawRenderedImage(final RenderedImage img, final AffineTransform xform)
  {}

  @Override
  public void drawRenderableImage(final RenderableImage img, final AffineTransform xform)
  {}

  @Override
  public void drawString(final String str, final float x, final float y)
  {
    drawText(x, y);
  }

  @Override
  public void drawString(final AttributedCharacterIterator iterator, final float x, final float y)
  {
    drawText(x, y);
  }

  @Override
  public void drawGlyphVector(final GlyphVector g, final float x, final float y)
  {
    drawText(x, y);
  }

  @Override
  public void fill(final Shape s)
  {}

  @Override
  public boolean hit(Rectangle rect, Shape shape, final boolean onStroke)
  {
    if (rect == null || shape == null)
    {
      return false;
    }

    if (onStroke && myStroke != null)
    {
      shape = myStroke.createStrokedShape(shape);
    }

    shape = myTransform.createTransformedShape(shape);

    if ((myClipBounds.x | myClipBounds.y) != 0)
    {
      if (myTmpRect1 == null)
      {
        myTmpRect1 = new Rectangle();
      }

      myTmpRect1.setBounds(rect);
      rect = myTmpRect1;
      rect.translate(myClipBounds.x, myClipBounds.y);
    }

    return shape.intersects(rect);
  }

  @Override
  public GraphicsConfiguration getDeviceConfiguration()
  {
    return myPeer.getDeviceConfiguration();
  }

  @Override
  public void setComposite(final Composite composite)
  {
    myComposite = composite;
  }

  @Override
  public void setPaint(final Paint paint)
  {
    myPaint = paint;
  }

  public void setStroke(final Stroke stroke)
  {
    myStroke = stroke;
  }

  @Override
  public void setRenderingHint(final RenderingHints.Key hintKey, final Object hintValue)
  {
    myRenderingHints.put(hintKey, hintValue);
  }

  @Override
  public Object getRenderingHint(final RenderingHints.Key hintKey)
  {
    return myRenderingHints.get(hintKey);
  }

  @Override
  public void setRenderingHints(final Map<?,?> hints)
  {
    myRenderingHints.clear();
    addRenderingHints(hints);
  }

  @Override
  public void addRenderingHints(final Map<?,?> hints)
  {
    myRenderingHints.putAll(hints);
  }

  @Override
  public RenderingHints getRenderingHints()
  {
    return myRenderingHints;
  }

  @Override
  public void translate(final double x, final double y)
  {
    myTransform.translate(x, y);
  }

  @Override
  public void rotate(final double theta)
  {
    myTransform.rotate(theta);
  }

  @Override
  public void rotate(final double theta, final double x, final double y)
  {
    myTransform.rotate(theta, x, y);
  }

  @Override
  public void scale(final double sx, final double sy)
  {
    myTransform.scale(sx, sy);
  }

  @Override
  public void shear(final double shx, final double shy)
  {
    myTransform.shear(shx, shy);
  }

  @Override
  public void transform(final AffineTransform tx)
  {
    myTransform.concatenate(tx);
  }

  @Override
  public void setTransform(final AffineTransform tx)
  {
    myTransform.setTransform(tx);
  }

  @Override
  public AffineTransform getTransform()
  {
    return myTransform;
  }

  @Override
  public Paint getPaint()
  {
    return myPaint;
  }

  @Override
  public Composite getComposite()
  {
    return myComposite;
  }

  @Override
  public void setBackground(final Color color)
  {
    myBackground = color;
  }

  @Override
  public Color getBackground()
  {
    return myBackground;
  }

  @Override
  public Stroke getStroke()
  {
    return myStroke;
  }

  @Override
  public void clip(final Shape shape)
  {
    if (shape == null)
    {
      return;
    }

    final Rectangle shapeBounds = shape.getBounds();
    clipRect(shapeBounds.x, shapeBounds.y, shapeBounds.width, shapeBounds.height);
  }

  @Override
  public FontRenderContext getFontRenderContext()
  {
    return myPeer.getFontRenderContext();
  }

  Point.Double getTextOrigin()
  {
    return mySharedData.myTextOrigin;
  }

  private void drawText(double x, double y)
  {
    x += myTransform.getTranslateX();
    y += myTransform.getTranslateY();

    Point.Double currTextOrigin = mySharedData.myTextOrigin;

    if (currTextOrigin == null)
    {
      mySharedData.myTextOrigin = new Point2D.Double(x, y);
      return;
    }

    if (x < currTextOrigin.x)
    {
      currTextOrigin.x = x;
    }

    if (y > currTextOrigin.y)
    {
      currTextOrigin.y = y;
    }
  }

  private static final class SharedData
  {
    private Point.Double myTextOrigin = null;
  }
}
