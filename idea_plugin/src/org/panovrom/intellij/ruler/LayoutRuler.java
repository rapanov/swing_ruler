/**
 * Copyright (C) 2019, Roman Panov (roman.a.panov@gmail.com).
 */

package org.panovrom.intellij.ruler;

import com.intellij.openapi.Disposable;
import com.intellij.openapi.ui.Painter;
import com.intellij.openapi.wm.IdeGlassPane;
import com.intellij.ui.JBColor;
import com.intellij.ui.SimpleColoredComponent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ContainerEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.AffineTransform;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ListCellRenderer;
import javax.swing.RootPaneContainer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.TreeCellRenderer;

final public class LayoutRuler
{
  private static final Color COMPONENT_RULER_COLOR = new JBColor(0x986C4A, 0x986C4A);
  private static final Color TEXT_RULER_COLOR = new JBColor(0x6C8A70, 0x6C8A70);

  private static final Color MISALIGNED_COMPONENT_COLOR = new Color(
      COMPONENT_RULER_COLOR.getRed(),
      COMPONENT_RULER_COLOR.getGreen(),
      COMPONENT_RULER_COLOR.getBlue(),
      100);

  private static final Color MISALIGNED_TEXT_COLOR = new Color(
      TEXT_RULER_COLOR.getRed(),
      TEXT_RULER_COLOR.getGreen(),
      TEXT_RULER_COLOR.getBlue(),
      100);

  private static final Color MISALIGNED_COMPONENT_AND_TEXT_COLOR = new Color(255, 0, 0, 100);
  private static final int MISALIGNMENT_SHIFT = 1;
  private static final String CELL_RENDERER_PROPERTY = "cellRenderer";

  private final IdeGlassPane myGlassPane;
  private final Component myContentPane;
  private final Painter myGlassPainter = new GlassPainter();
  private final GlassMouseListener myGlassMouseListener = new GlassMouseListener();
  private final ContainerListener myContainerListener = new ContainerListener();
  private final DoubleBufferingDisabler myDoubleBufferingDisabler = new DoubleBufferingDisabler();
  private final ComponentsInfo myRenderedComponents = new ComponentsInfo();
  private final ListTailor myListTailor = new ListTailor();
  private final TreeTailor myTreeTailor = new TreeTailor();
  private final TableTailor myTableTailor = new TableTailor();
  private final Rectangle myTmpRect = new Rectangle();
  private final Point myTmpXY = new Point();
  private boolean myIsRefreshing = false;
  private boolean myIsDetached = false;
  private int myMouseX = -1;
  private int myMouseY = -1;


  public static LayoutRuler attach(final Window window)
  {
    if (window instanceof RootPaneContainer)
    {
      final RootPaneContainer rootPaneContainer = (RootPaneContainer)window;

      if (
          rootPaneContainer.getGlassPane() instanceof IdeGlassPane &&
          rootPaneContainer.getContentPane() != null)
      {
        return new LayoutRuler(rootPaneContainer);
      }
    }

    return null;
  }

  public boolean isDetached()
  {
    return myIsDetached;
  }

  public void detach()
  {
    if (myIsDetached)
    {
      return;
    }

    try
    {
      myGlassPane.removeMouseMotionPreprocessor(myGlassMouseListener);
      myGlassPane.removePainter(myGlassPainter);
      uninstall(myContentPane);
      myListTailor.undoTailor();
      myTreeTailor.undoTailor();
      myTableTailor.undoTaylor();
    }
    finally
    {
      myIsDetached = true;
    }
  }

  private LayoutRuler(final RootPaneContainer rootPaneContainer)
  {
    final Object glassPane = rootPaneContainer.getGlassPane();
    assert rootPaneContainer instanceof Component;
    assert glassPane instanceof IdeGlassPane;

    myContentPane = rootPaneContainer.getContentPane();
    myGlassPane = (IdeGlassPane)glassPane;
    final Disposable disposer = new Disposer();

    install(myContentPane);
    myGlassPane.addPainter(myContentPane, myGlassPainter, disposer);
    myGlassPane.addMouseMotionPreprocessor(myGlassMouseListener, disposer);
  }

  private void setMousePos(final int x, final int y)
  {
    boolean changed = false;

    if (x != myMouseX)
    {
      myMouseX = x;
      changed = true;
    }

    if (y != myMouseY)
    {
      myMouseY = y;
      changed = true;
    }

    if (changed)
    {
      refresh();
    }
  }

  private void refresh()
  {
    if (myIsRefreshing)
    {
      return;
    }

    myIsRefreshing = true;
    myRenderedComponents.clear();
    myContentPane.repaint();
  }

  private void paintGlass(final Component component, final Graphics2D graphics)
  {
    if (myIsDetached)
    {
      return;
    }

    final int contentWidth = component.getWidth();
    final int contentHeight = component.getHeight();

    if (contentWidth <= 0 || contentHeight <= 0)
    {
      myIsRefreshing = false;
      return;
    }

    final boolean toRefresh;

    if (myIsRefreshing)
    {
      // OK, the components are rendered, and we gotta render the glass bar.
      myIsRefreshing = false;
      addRenderedComponents(myContentPane, null, null, 0, 0, 0);
      toRefresh = false;
    }
    else
    {
      // This repaint is not requested by my refresh.
      // It means something is changed and we need to refresh our stuff.
      toRefresh = true;
    }

    paintGlass(graphics, contentWidth, contentHeight);

    if (toRefresh)
    {
      refresh();
    }
  }

  private void paintGlass(
      final Graphics2D graphics,
      final int contentWidth, final int contentHeight)
  {
    final ComponentInfo activeComp = myRenderedComponents.getActive();

    if (activeComp == null || activeComp.myWidth <= 0 || activeComp.myHeight <= 0)
    {
      return;
    }

    final int activeCompX = activeComp.myX;
    final int activeCompBaseline = activeComp.myY + activeComp.myHeight;
    int activeTextX = activeComp.myTextX;
    int activeTextBaseline = activeComp.myTextBaseline;

    if (activeTextX >= 0)
    {
      activeTextX += activeComp.myX;
    }

    if (activeTextBaseline >= 0)
    {
      activeTextBaseline += activeComp.myY;
    }

    for (final ComponentInfo comp : myRenderedComponents.myComponents)
    {
      if (comp == activeComp)
      {
        continue;
      }

      boolean isCompMisaligned = isMisaligned(comp.myX, activeCompX);

      if (!isCompMisaligned)
      {
        isCompMisaligned = isMisaligned(comp.myY + comp.myHeight, activeCompBaseline);
      }

      boolean isTextMisaligned = false;

      if (comp.myTextX >= 0 && activeTextX >= 0)
      {
        isTextMisaligned = isMisaligned(comp.myTextX + comp.myX, activeTextX);
      }

      if (!isTextMisaligned && comp.myTextBaseline >= 0 && activeTextBaseline >= 0)
      {
        isTextMisaligned = isMisaligned(comp.myTextBaseline + comp.myY, activeTextBaseline);
      }

      Color color = null;

      if (isCompMisaligned)
      {
        color = isTextMisaligned ? MISALIGNED_COMPONENT_AND_TEXT_COLOR : MISALIGNED_COMPONENT_COLOR;
      }
      else if (isTextMisaligned)
      {
        color = MISALIGNED_TEXT_COLOR;
      }

      if (color != null)
      {
        graphics.setColor(color);
        graphics.fillRect(comp.myX, comp.myY, comp.myWidth, comp.myHeight);
      }
    }

    if (activeCompX > 0 && activeCompX <= contentWidth)
    {
      graphics.setColor(COMPONENT_RULER_COLOR);
      graphics.fillRect(activeCompX - 1, 0, 1, contentHeight);
    }

    if (activeCompBaseline >= 0 && activeCompBaseline < contentHeight)
    {
      graphics.setColor(COMPONENT_RULER_COLOR);
      graphics.fillRect(0, activeCompBaseline, contentWidth, 1);
    }

    if (activeTextX != activeCompX && activeTextX > 0 && activeTextX <= contentWidth)
    {
      graphics.setColor(TEXT_RULER_COLOR);
      graphics.fillRect(activeTextX - 1, 0, 1, contentHeight);
    }

    if (activeTextBaseline != activeCompBaseline &&
        activeTextBaseline >= 0 && activeTextBaseline < contentHeight)
    {
      graphics.setColor(TEXT_RULER_COLOR);
      graphics.fillRect(0, activeTextBaseline, contentWidth, 1);
    }
  }

  private static boolean isMisaligned(final int x0, final int x1)
  {
    return x0 != x1 && Math.abs(x0 - x1) <= MISALIGNMENT_SHIFT;
  }

  private void mouseMoved(final MouseEvent e)
  {
    if (!myIsDetached)
    {
      setMousePos(e.getX(), e.getY());
    }
  }

  private void componentAdded(final ContainerEvent e)
  {
    if (!myIsDetached)
    {
      install(e.getChild());
    }
  }

  private void componentRemoved(final ContainerEvent e)
  {
    if (myIsDetached)
    {
      return;
    }

    final Component component = e.getChild();

    if (component.getParent() == null)
    {
      uninstall(component);
    }
  }

  private void addRenderedComponents(
      final Component component,
      final Container parent,
      Rectangle viewRect,
      final int origX,
      final int origY,
      final int rootDistance)
  {
    if (component == null || !component.isVisible())
    {
      return;
    }

    final Rectangle compBounds = component.getBounds(myTmpRect);

    if (compBounds.width <= 0 || compBounds.height <= 0)
    {
      return;
    }

    compBounds.x = origX;
    compBounds.y = origY;

    if (viewRect == null)
    {
      viewRect = new Rectangle(compBounds);
    }
    else
    {
      viewRect = viewRect.intersection(compBounds);
    }

    if (viewRect.width <= 0 || viewRect.height <= 0)
    {
      // Completely out of viewport.
      return;
    }

    if (shallRender(component))
    {
      int zOrder = -1;

      if (parent != null && component.getParent() == parent)
      {
        zOrder = parent.getComponentZOrder(component);
      }

      boolean isActive =
          myMouseX >= 0 && myMouseY >= 0 && compBounds.contains(myMouseX, myMouseY);

      if (isActive)
      {
        final ComponentInfo activeComp = myRenderedComponents.getActive();

        if (activeComp != null)
        {
          if (activeComp.myRootDistance > rootDistance)
          {
            isActive = false;
          }
          else if (activeComp.myRootDistance == rootDistance)
          {
            if (activeComp.myZOrder < zOrder)
            {
              isActive = false;
            }
          }
        }
      }

      final ComponentInfo compInfo = myRenderedComponents.add(isActive);
      compInfo.setBounds(compBounds);
      compInfo.myRootDistance = rootDistance;
      compInfo.myZOrder = zOrder;
      setTextRenderingInfo(component, compInfo);
      return;
    }

    if (component instanceof Container)
    {
      final Container container = (Container)component;

      synchronized (container.getTreeLock())
      {
        final int childrenCount = container.getComponentCount();

        for (int i = 0; i < childrenCount; ++i)
        {
          final Component child = container.getComponent(i);
          addRenderedComponents(
              child, container, viewRect,
              origX + child.getX(),
              origY + child.getY(),
              rootDistance + 1);
        }
      }
    }
  }

  private static boolean shallRender(final Object component)
  {
    return
        component instanceof JButton ||
        component instanceof JSlider ||
        component instanceof JLabel ||
        component instanceof JCheckBox ||
        component instanceof JComboBox ||
        component instanceof JRadioButton ||
        component instanceof JTextField ||
        component instanceof JSpinner ||
        component instanceof SimpleColoredComponent;
  }

  private void setTextRenderingInfo(
      final Component component, final ComponentInfo compInfo)
  {
    compInfo.resetTextRenderingInfo();

    if (!(component instanceof JComponent))
    {
      return;
    }

    final boolean hasText =
        component instanceof JLabel ||
        component instanceof JButton ||
        component instanceof JCheckBox ||
        component instanceof JComboBox ||
        component instanceof JRadioButton ||
        component instanceof JTextField ||
        component instanceof JSpinner ||
        component instanceof SimpleColoredComponent;

    if (!hasText)
    {
      return;
    }

    final int width = component.getWidth();
    final int height = component.getHeight();

    final Graphics graphics = component.getGraphics().create(0, 0, width, height);

    if (!(graphics instanceof Graphics2D))
    {
      return;
    }

    final Graphics2D graphics2D = (Graphics2D)graphics;
    final AffineTransform origTransform = graphics2D.getTransform();
    final double origTx = origTransform.getTranslateX();
    final double origTy = origTransform.getTranslateY();
    final Graphics2DWrapper graphicsWrapper = new Graphics2DWrapper((Graphics2D)graphics);

    try
    {
      myDoubleBufferingDisabler.disable((JComponent)component);

      try
      {
        component.paint(graphicsWrapper);
      }
      finally
      {
        graphics.dispose();
      }
    }
    finally
    {
      myDoubleBufferingDisabler.restore();
    }

    final Point.Double textOrigin = graphicsWrapper.getTextOrigin();

    if (textOrigin == null)
    {
      return;
    }

    textOrigin.x -= origTx;
    textOrigin.y -= origTy;

    final int textX = (int)textOrigin.x;
    final int textBaseline = (int)textOrigin.y;

    if (textX >= 0 && textX < width)
    {
      compInfo.myTextX = textX;
    }

    if (textBaseline > 0 && textBaseline <= height)
    {
      compInfo.myTextBaseline = textBaseline;
    }
  }

  private int computeRootDistance(Component component)
  {
    int distance = 0;

    while (component != null)
    {
      if (component == myContentPane)
      {
        break;
      }

      ++distance;
      component = component.getParent();
    }

    return distance;
  }

  private boolean computeContentPaneXY(Component component, final Point xy)
  {
    while (component != null && component != myContentPane)
    {
      xy.x += component.getX();
      xy.y += component.getY();

      component = component.getParent();
    }

    return  component != null;
  }

  private void install(final Component component)
  {
    installOrUninstall(component, true);
  }

  private void uninstall(final Component component)
  {
    installOrUninstall(component, false);
  }

  private void installOrUninstall(final Component component, final boolean toInstall)
  {
    if (toInstall)
    {
      if (component instanceof JList)
      {
        myListTailor.doTailor((JList)component);
      }
      else if (component instanceof JTree)
      {
        myTreeTailor.doTailor((JTree)component);
      }
      else if (component instanceof JTable)
      {
        myTableTailor.doTailor((JTable)component);
      }
    }

    if (component instanceof Container)
    {
      final Container container = (Container)component;

      if (toInstall)
      {
        final Object[] listeners = container.getContainerListeners();
        boolean isListenerInstalled = false;

        for (final Object listener : listeners)
        {
          if (listener == myContainerListener)
          {
            isListenerInstalled = true;
            break;
          }
        }

        if (!isListenerInstalled)
        {
          container.addContainerListener(myContainerListener);
        }
      }
      else
      {
        container.removeContainerListener(myContainerListener);
      }

      synchronized (component.getTreeLock())
      {
        final int compCount = container.getComponentCount();

        for (int i = 0; i < compCount; ++i)
        {
          installOrUninstall(container.getComponent(i), toInstall);
        }
      }
    }
  }

  private final class CellWrapper extends JComponent
  {
    private final Component myPeer;

    @Override
    public void paint(final Graphics graphics)
    {
      super.paint(graphics);
      final Point orig = myTmpXY;
      orig.setLocation(0, 0);

      if (computeContentPaneXY(this, orig))
      {
        addRenderedComponents(myPeer, this, null, orig.x, orig.y, computeRootDistance(this));
      }
    }

    private CellWrapper(final Component peer)
    {
      assert peer != null;
      myPeer = peer;
      setLayout(new LayoutManager());
    }

    private final class LayoutManager implements java.awt.LayoutManager
    {
      @Override
      public void addLayoutComponent(final String name, final Component component)
      {}

      @Override
      public void removeLayoutComponent(final Component component)
      {}

      @Override
      public Dimension preferredLayoutSize(final Container container)
      {
        return myPeer.getPreferredSize();
      }

      @Override
      public Dimension minimumLayoutSize(final Container container)
      {
        return myPeer.getMaximumSize();
      }

      @Override
      public void layoutContainer(final Container container)
      {
        if (myPeer.getParent() == CellWrapper.this)
        {
          myPeer.setLocation(0, 0);
          myPeer.setSize(container.getWidth(), container.getHeight());
        }
      }
    }
  }

  private final class ListTailor
  {
    private final Map<JList<?>, ListCellRenderer> myTailoredLists =
        new HashMap<JList<?>, ListCellRenderer>();
    private final PropertyChangeListener myRendererChangeListener = new RendererChangeListener();

    // Flag to prevent indefinite recursion because of property change event.
    private boolean myIsSettingRenderer = false;

    private void doTailor(final JList<?> list)
    {
      if (myTailoredLists.containsKey(list))
      {
        return;
      }

      wrapRenderer(list);
      list.addPropertyChangeListener(CELL_RENDERER_PROPERTY, myRendererChangeListener);
    }

    @SuppressWarnings("unchecked")
    private void undoTailor()
    {
      for (final Map.Entry<JList<?>, ListCellRenderer> entry : myTailoredLists.entrySet())
      {
        final JList<?> list = entry.getKey();
        list.removePropertyChangeListener(CELL_RENDERER_PROPERTY, myRendererChangeListener);
        list.setCellRenderer(entry.getValue());
      }
    }

    @SuppressWarnings("unchecked")
    private void wrapRenderer(final JList<?> list)
    {
      if (myIsSettingRenderer)
      {
        return;
      }

      final ListCellRenderer origRenderer = list.getCellRenderer();

      if (origRenderer == null)
      {
        return;
      }

      try
      {
        myIsSettingRenderer = true;
        list.setCellRenderer(new CellRendererWrapper(origRenderer));
        myTailoredLists.put(list, origRenderer);
      }
      finally
      {
        myIsSettingRenderer = false;
      }
    }

    private final class CellRendererWrapper<E> implements ListCellRenderer<E>
    {
      private final ListCellRenderer<E> myPeer;

      @Override
      public Component getListCellRendererComponent(
          final JList<? extends E> list, final E value, final int index,
          final boolean isSelected, final boolean cellHasFocus)
      {
        final Component origCell =
            myPeer.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

        if (origCell == null)
        {
          return null;
        }

        final Container cellWrapper = new CellWrapper(origCell);
        cellWrapper.add(BorderLayout.CENTER, origCell);
        return cellWrapper;
      }

      private CellRendererWrapper(final ListCellRenderer<E> peer)
      {
        assert peer != null;
        myPeer = peer;
      }
    }

    private final class RendererChangeListener implements PropertyChangeListener
    {
      @Override
      public void propertyChange(final PropertyChangeEvent e)
      {
        if (myIsDetached || myIsSettingRenderer)
        {
          return;
        }

        if (!CELL_RENDERER_PROPERTY.equals(e.getPropertyName()))
        {
          return;
        }

        final Object list = e.getSource();
        final Object newRenderer = e.getNewValue();

        if (list instanceof JList && !(newRenderer instanceof CellRendererWrapper))
        {
          wrapRenderer((JList)list);
        }
      }
    }
  }

  private final class TreeTailor
  {
    private final Map<JTree, TreeCellRenderer> myTailoredLists =
        new HashMap<JTree, TreeCellRenderer>();
    private final PropertyChangeListener myRendererChangeListener = new RendererChangeListener();

    // Flag to prevent indefinite recursion because of property change event.
    private boolean myIsSettingRenderer = false;

    private void doTailor(final JTree tree)
    {
      if (myTailoredLists.containsKey(tree))
      {
        return;
      }

      wrapRenderer(tree);
      tree.addPropertyChangeListener(CELL_RENDERER_PROPERTY, myRendererChangeListener);
    }

    private void undoTailor()
    {
      for (final Map.Entry<JTree, TreeCellRenderer> entry : myTailoredLists.entrySet())
      {
        final JTree tree = entry.getKey();
        tree.removePropertyChangeListener(CELL_RENDERER_PROPERTY, myRendererChangeListener);
        tree.setCellRenderer(entry.getValue());
      }
    }

    private void wrapRenderer(final JTree tree)
    {
      if (myIsSettingRenderer)
      {
        return;
      }

      final TreeCellRenderer origRenderer = tree.getCellRenderer();

      if (origRenderer == null)
      {
        return;
      }

      try
      {
        myIsSettingRenderer = true;
        tree.setCellRenderer(new CellRendererWrapper(origRenderer));
        myTailoredLists.put(tree, origRenderer);
      }
      finally
      {
        myIsSettingRenderer = false;
      }
    }

    private final class CellRendererWrapper implements TreeCellRenderer
    {
      private final TreeCellRenderer myPeer;

      @Override
      public Component getTreeCellRendererComponent(
          final JTree tree, final Object value,
          final boolean selected, final boolean expanded,
          final boolean leaf, final int row, final boolean hasFocus)
      {
        final Component origCell = myPeer.getTreeCellRendererComponent(
            tree, value, selected, expanded, leaf, row, hasFocus);

        if (origCell == null)
        {
          return null;
        }

        final Container cellWrapper = new CellWrapper(origCell);
        cellWrapper.add(BorderLayout.CENTER, origCell);
        return cellWrapper;
      }

      private CellRendererWrapper(final TreeCellRenderer peer)
      {
        assert peer != null;
        myPeer = peer;
      }
    }

    private final class RendererChangeListener implements PropertyChangeListener
    {
      @Override
      public void propertyChange(PropertyChangeEvent e)
      {
        if (myIsDetached || myIsSettingRenderer)
        {
          return;
        }

        if (!CELL_RENDERER_PROPERTY.equals(e.getPropertyName()))
        {
          return;
        }

        final Object tree = e.getSource();
        final Object newRenderer = e.getNewValue();

        if (tree instanceof JTree && !(newRenderer instanceof CellRendererWrapper))
        {
          wrapRenderer((JTree)tree);
        }
      }
    }
  }

  private final class TableTailor
  {
    private final Set<TableColumnModel> myTailoredColumnModels = new HashSet<TableColumnModel>();
    private final Map<TableColumn, TableCellRenderer> myTailoredColumns =
        new HashMap<TableColumn, TableCellRenderer>();
    private final PropertyChangeListener myRendererChangeListener = new RendererChangeListener();
    private final TableColumnModelListener myColumnModelListener = new ColumnModelListener();

    // Flag to prevent indefinite recursion because of property change event.
    private boolean myIsSettingRenderer = false;

    private void doTailor(final JTable table)
    {
      final TableColumnModel columnModel = table.getColumnModel();

      if (columnModel == null)
      {
        return;
      }

      final int columnCount = columnModel.getColumnCount();

      if (!myTailoredColumnModels.contains(columnModel))
      {
        columnModel.addColumnModelListener(myColumnModelListener);
        myTailoredColumnModels.add(columnModel);
      }

      for (int i = 0; i < columnCount; ++i)
      {
        doTailor(columnModel.getColumn(i));
      }
    }

    private void doTailor(final TableColumn column)
    {
      if (myTailoredColumns.containsKey(column))
      {
        return;
      }

      wrapRenderer(column);
      column.addPropertyChangeListener(myRendererChangeListener);
    }

    private void undoTaylor()
    {
      for (final TableColumnModel columnModel : myTailoredColumnModels)
      {
        columnModel.removeColumnModelListener(myColumnModelListener);
      }

      for (final Map.Entry<TableColumn, TableCellRenderer> entry : myTailoredColumns.entrySet())
      {
        final TableColumn column = entry.getKey();
        column.removePropertyChangeListener(myRendererChangeListener);
        column.setCellRenderer(entry.getValue());
      }
    }

    private void wrapRenderer(final TableColumn column)
    {
      if (myIsSettingRenderer)
      {
        return;
      }

      final TableCellRenderer origRenderer = column.getCellRenderer();

      if (origRenderer == null)
      {
        return;
      }

      try
      {
        myIsSettingRenderer = true;
        column.setCellRenderer(new CellRendererWrapper(origRenderer));
        myTailoredColumns.put(column, origRenderer);
      }
      finally
      {
        myIsSettingRenderer = false;
      }
    }

    private final class CellRendererWrapper implements TableCellRenderer
    {
      private final TableCellRenderer myPeer;

      @Override
      public Component getTableCellRendererComponent(
          final JTable table, final Object value,
          final boolean selected, final boolean hasFocus,
          final int row, final int column)
      {
        final Component origCell = myPeer.getTableCellRendererComponent(
            table, value, selected, hasFocus, row, column);

        if (origCell == null)
        {
          return null;
        }

        final Container cellWrapper = new CellWrapper(origCell);
        cellWrapper.add(BorderLayout.CENTER, origCell);
        return cellWrapper;
      }

      private CellRendererWrapper(final TableCellRenderer peer)
      {
        assert peer != null;
        myPeer = peer;
      }
    }

    private final class RendererChangeListener implements PropertyChangeListener
    {
      @Override
      public void propertyChange(PropertyChangeEvent e)
      {
        if (myIsDetached || myIsSettingRenderer)
        {
          return;
        }

        if (!CELL_RENDERER_PROPERTY.equals(e.getPropertyName()))
        {
          return;
        }

        final Object column = e.getSource();
        final Object newRenderer = e.getNewValue();

        if (column instanceof TableColumn && !(newRenderer instanceof CellRendererWrapper))
        {
          wrapRenderer((TableColumn)column);
        }
      }
    }

    private final class ColumnModelListener implements TableColumnModelListener
    {
      @Override
      public void columnAdded(final TableColumnModelEvent e)
      {
        if (myIsDetached)
        {
          return;
        }

        final Object column = e.getSource();

        if (column instanceof TableColumn)
        {
          doTailor((TableColumn)column);
        }
      }

      @Override
      public void columnRemoved(final TableColumnModelEvent e)
      {}

      @Override
      public void columnMoved(final TableColumnModelEvent e)
      {}

      @Override
      public void columnMarginChanged(final ChangeEvent e)
      {}

      @Override
      public void columnSelectionChanged(final ListSelectionEvent e)
      {}
    }
  }

  private static final class DoubleBufferingDisabler
  {
    private List<JComponent> myAffectedComponents = new ArrayList<JComponent>();

    private void disable(final JComponent component)
    {
      if (component == null)
      {
        return;
      }

      if (component.isDoubleBuffered())
      {
        component.setDoubleBuffered(false);
        myAffectedComponents.add(component);
      }

      synchronized (component.getTreeLock())
      {
        final int childrenCount = component.getComponentCount();

        for (int i = 0; i < childrenCount; ++i)
        {
          final Component child = component.getComponent(i);

          if (child instanceof JComponent)
          {
            disable((JComponent)child);
          }
        }
      }
    }

    void restore()
    {
      int idx = myAffectedComponents.size();

      while (idx > 0)
      {
        --idx;
        final JComponent component = myAffectedComponents.get(idx);

        if (component != null)
        {
          component.setDoubleBuffered(true);
        }

        myAffectedComponents.remove(idx);
      }
    }
  }

  private static final class ComponentInfo
  {
    private int myX;
    private int myY;
    private int myWidth;
    private int myHeight;
    private int myTextX;
    private int myTextBaseline;
    private int myRootDistance;
    private int myZOrder;

    private ComponentInfo()
    {
      reset();
    }

    private void reset()
    {
      myX = -1;
      myY = -1;
      myWidth = 0;
      myHeight = 0;
      myTextX = -1;
      myTextBaseline = -1;
      myRootDistance = -1;
      myZOrder = -1;
    }

    private void resetTextRenderingInfo()
    {
      myTextX = -1;
      myTextBaseline = -1;
    }

    private void setBounds(final Rectangle bounds)
    {
      myX = bounds.x;
      myY = bounds.y;
      myWidth = bounds.width;
      myHeight = bounds.height;
    }
  }

  private static final class ComponentsInfo
  {
    private final List<ComponentInfo> myComponents = new ArrayList<ComponentInfo>();
    private final List<ComponentInfo> myPool = new ArrayList<ComponentInfo>();
    private int myActiveIndex = -1;

    private ComponentInfo getActive()
    {
      if (myActiveIndex >= 0 && myActiveIndex < myComponents.size())
      {
        return myComponents.get(myActiveIndex);
      }

      return null;
    }

    private ComponentInfo add(boolean isActive)
    {
      final int currCount = myComponents.size();

      if (currCount >= myPool.size())
      {
        myPool.add(new ComponentInfo());
        assert currCount < myPool.size();
      }

      final ComponentInfo compInfo = myPool.get(currCount);
      myComponents.add(compInfo);

      if (isActive)
      {
        myActiveIndex = currCount;
      }

      return compInfo;
    }

    private void clear()
    {
      myComponents.clear();
      myActiveIndex = -1;
    }
  }

  private final class ContainerListener implements java.awt.event.ContainerListener
  {
    @Override
    public void componentAdded(final ContainerEvent e)
    {
      LayoutRuler.this.componentAdded(e);
    }

    @Override
    public void componentRemoved(final ContainerEvent e)
    {
      LayoutRuler.this.componentRemoved(e);
    }
  }

  private final class GlassPainter implements Painter
  {
    @Override
    public boolean needsRepaint()
    {
      return !myIsDetached;
    }

    @Override
    public void paint(final Component component, final Graphics2D graphics)
    {
      paintGlass(component, graphics);
    }

    @Override
    public void addListener(final Painter.Listener listener)
    {}

    @Override
    public void removeListener(final Painter.Listener listener)
    {}
  }

  private final class GlassMouseListener extends MouseMotionAdapter
  {
    @Override
    public void mouseMoved(final MouseEvent e)
    {
      LayoutRuler.this.mouseMoved(e);
    }
  }

  private static final class Disposer implements Disposable
  {
    @Override
    public void dispose()
    {}
  }
}
